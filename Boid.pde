public class Boid extends VBoid
{
  public PShape shape;

  public Boid(Vec pos, Vec vel)
  {
    super(pos, vel);

    this.swarm.setSeperationScale(2f);
    this.swarm.setAlignScale(0.5f);
    this.swarm.setCohesionScale(0.5f);

    this.swarm.setAlignRadius(25f);
    this.swarm.setSeperationRadius(100.0f);
    this.swarm.setCohesionRadius(25f);
    //this.setFriction(0.01f);

    noStroke();
    fill(255); 
    //sphereDetail(40);

    shape = createShape(RECT, -3, -3, 6, 6);
    shape.setTexture(loadImage("moi2.jpg"));
  }
}

