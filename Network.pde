public class Network
{
  PeasyCam cam;
  protected ParticleManager particlesMgr;

  protected Lover[] lovers;
  public int nbLovers = 0;
  protected int currentLoverIndex = -1;

  protected int NB_MAX_LOVERS = 10000;
  protected int NB_MAX_RELATIONS = 10000;

  boolean cameraMode = false;

  public Network(PApplet applet)
  {
    cam = new PeasyCam(applet, 300);
    cam.setActive(cameraMode);
    //cam.setMinimumDistance(1); dies not work ?
    lovers = new Lover[NB_MAX_LOVERS];
    particlesMgr = new ParticleManager(NB_MAX_LOVERS);

    for (int i = 0; i < 100; i++) createLover("lover"+str(nbLovers));
    for (int i = 0; i < 500; i++) particlesMgr.createRandomLink();
  } 

  public void switchCameraMode()
  {
    cameraMode = !cameraMode;
    cam.setActive(cameraMode);
  }

  public void createLover(String id)
  {
    lovers[nbLovers++] = new Lover(id);
    particlesMgr.createBoid();
  }

  public void selectLover(int index)
  {
    if (nbLovers <= 0) 
    {
      currentLoverIndex = -1;
      return;
    }
    while (index < 0) index += nbLovers;
    index = index%nbLovers;   
    currentLoverIndex = index%nbLovers;

    particlesMgr.selectBoid(index);
    cam.lookAt(particlesMgr.selectedParticle.x, particlesMgr.selectedParticle.y, particlesMgr.selectedParticle.z, 100.0f);
  }

  public void update()
  {
    particlesMgr.update();
    
  }

  public void display()
  { 
    //if (particlesMgr.selectedParticle != null)
    //  cam.lookAt(particlesMgr.selectedParticle.x, particlesMgr.selectedParticle.y, particlesMgr.selectedParticle.z);
    
    background(cameraMode?0:30);
    
    float[] rotations = cam.getRotations();

    strokeWeight(1);
    stroke(255, 0, 0, 50);
    for (VSpring s : particlesMgr.notSelectedLinks)
      line(s.a.x, s.a.y, s.a.z, s.b.x, s.b.y, s.b.z);

    strokeWeight(2);
    stroke(0, 255, 0, 255);
    for (VSpring s : particlesMgr.selectedLinks)
      line(s.a.x, s.a.y, s.a.z, s.b.x, s.b.y, s.b.z);


    strokeWeight(5);
    stroke(255);
    for (int i = 0; i < particlesMgr.nbParticles; i++)
    {
      Boid boid = (Boid)particlesMgr.particles[i];
      
      pushMatrix();
      translate(boid.x, boid.y, boid.z);
    rotateX(rotations[0]);
    rotateY(rotations[1]);
    rotateZ(rotations[2]);
      
      if (i == currentLoverIndex)
      {
        strokeWeight(10);
        stroke(255, 255, 0);
        //point(boid.x, boid.y, boid.z);
        //point(0, 0, 0);
        shape(boid.shape);
        strokeWeight(5);
        stroke(255);
      } 
      else
        //point(boid.x, boid.y, boid.z);
        //point(0, 0, 0);
        shape(boid.shape);
        
        popMatrix();
    }

    strokeWeight(1);
    noFill();
    stroke(0, 0, 255, 100);
    box(300);

  }
}

