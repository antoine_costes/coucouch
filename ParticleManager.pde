public class ParticleManager
{

  protected VPhysics physics;
  protected VParticle center = new VParticle();
  protected VSpringAnchor selectSpring;
  public VParticle selectedParticle;

  protected PVector minBoudaries;
  protected PVector maxBoudaries;

  public VParticle[] particles;
  public int nbParticles;

  public VSpring[] links;
  public int nbLinks;

  public ArrayList<VSpring> selectedLinks;
  public ArrayList<VSpring> notSelectedLinks;

  public ParticleManager(int NB_MAX_PARTICLES)
  {
    this(NB_MAX_PARTICLES, new PVector(-100, -100, -100), new PVector(100, 100, 100));
  }

  public ParticleManager(int NB_MAX_PARTICLES, PVector minBoudaries, PVector maxBoudaries)
  {
    this.minBoudaries = minBoudaries;
    this.maxBoudaries = maxBoudaries;

    physics = new VPhysics();
    particles = new VParticle[NB_MAX_PARTICLES];
    links = new VSpring[NB_MAX_PARTICLES];
    selectedLinks = new ArrayList<VSpring>();
    notSelectedLinks = new ArrayList<VSpring>();

    center.lock();
    selectSpring = new VSpringAnchor(center, center, 0.0f, 0.005f);
    physics.addSpring(selectSpring);
  }

  public void update()
  {
    physics.update();
  }

  public int getNbPart()
  {
    return physics.particles.size();
  }

  public VParticle[] getParticles()
  {
    return particles;
  }

  public VSpring[] getSprings()
  {
    return links;
  }

  public void createRandomLink()
  {
    int index1 = int(random(physics.particles.size()));
    int index2 = int(random(physics.particles.size()));
    this.createLink(index1, index2);
  }

  public void createLink(int index1, int index2)
  {
    this.createLink(physics.particles.get(index1), physics.particles.get(index2));
  }

  public void createLink(VParticle part1, VParticle part2)
  {
    // physics.addSpring(new VSpringRange(p, center, 150, 250, 0.0003f));
    VSpring link = new VSpring(part1, part2, 10f, 0.00001f);
    physics.addSpring(link);
    links[nbLinks++] = link;
    notSelectedLinks.add(link);
  }

  public void createBoid()
  {
    // instantiate boid
    Vec pos = new Vec(random(minBoudaries.x, maxBoudaries.x), random(minBoudaries.y, maxBoudaries.y), random(minBoudaries.z, maxBoudaries.z));
    Vec vel = new Vec(random(-1, 1), random(-1, 1), random(-1, 1));
    Boid newBoid = new Boid(pos, vel);
    
    // add particle to physics with an anchor to center
    physics.addParticle(newBoid);
    particles[nbParticles++] = newBoid;
    VSpring anchor = new VSpring(center, newBoid, 50.0f, .00001f);
    physics.addSpring(anchor);
  }

  public void selectBoid(int index)
  {
    if (index < 0 || index > physics.particles.size()) return;
    
    if (selectedParticle != null) selectedParticle.setFriction(0.0f);
    selectedParticle = physics.particles.get(index); 
    selectedParticle.setFriction(1.5f);
    //selectSpring.setParticle(selectedParticle);

    selectedLinks = new ArrayList<VSpring>();
    notSelectedLinks = new ArrayList<VSpring>();
    //for (VSpring s : physics.springs)
    for (int i = 0 ; i < nbLinks ; i++)
    {
      VSpring s = links[i];
      if (s.a == selectedParticle || s.b == selectedParticle)
      {
        s.setStrength(0.0005f);
        selectedLinks.add(s);
      }
      else
      {
        s.setStrength(0.00001f);
        notSelectedLinks.add(s); 
      }
    }
  }
}

