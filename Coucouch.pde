import peasy.PeasyCam;
import punktiert.math.Vec;
import punktiert.physics.*;

Network network;

int wheelCount = 0;


public void setup()
{
  size(800, 600, P3D);
  smooth();

  network = new Network(this);
 
}

public void draw()
{
  network.update();
  network.display();
  text(frameRate, 0, 0);//-height/2 + 10);
}

void keyPressed()
{
  switch (key)
  {
  case ' ':
    network.switchCameraMode();
    break;
  }
}

void mouseWheel(MouseEvent event)
{
  wheelCount += int(event.getCount());

  if (!network.cameraMode)
    network.selectLover(wheelCount);
}

